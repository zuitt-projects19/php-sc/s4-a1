<?php

class Building{
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}

	public function setName($name){
		$this->name = $name;
		}
};

class Condominium extends Building{
	// public function getName(){
	// 	return $this->name;
	// }

	// public function setName($name){
	// 	$this->name = $name;
	// }

	// public function getFloors(){
	// 	return $this->floors;
	// }

	// public function getAddress(){
	// 	return $this->address;
	// }
}

class Apartment extends Building{
	/*
	//getter
	public function getName(){
		return $this->name;
	}

	//setter
	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}
	*/
};

$building = new Building("Caswynn Building", 8, "Quezon City, Philippines");
$condominium = new Condominium("Enzo Condo", 5, "Makati City, Philippines");
$apartment = new Apartment("Enzo Condo", 5, "Makati City, Philippines");
