<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S4: Access Modifiers and Inheritance</title>
</head>
<body>
	<h1>Access Modifiers</h1>

	<h2>Building Variables</h2>
	<?php // echo $condominium->name; ?>
	<p>You should see "Cannot access protected property Condominium::$name" when trying to directly access a protected property.</p>

	<h1>Encapsulation</h1>
	<p><?= $condominium->getName(); ?></p>

	<?php //$apartment->name = "Enzo Apartment"; ?>

	<?php $apartment->setName('Enzo Apartment') ; ?>

	<p>The name of the apartment has been changed to <?= $apartment->getName(); ?></p>

	<hr>

	<h1>Activity</h1>
	<h2>Building</h2>
	<?php $building->getName() ; ?>
	<p>The name of the building is <?= $building->getName(); ?></p>
	<p><?= $building->getName(); ?> has <?= $building->getFloors();?> floors</p>
	<p><?= $building->getName(); ?> is located at <?= $building->getAddress();?></p>

	<?php $building->setName('Caswynn Complex') ; ?>
	<p>The name of the building has been changed to <?= $building->getName(); ?></p>

	<h2>Condominium</h2>
	<?php $condominium->getName() ; ?>
	<p>The name of the building is <?= $condominium->getName(); ?></p>
	<p><?= $condominium->getName(); ?> has <?= $condominium->getFloors();?> floors</p>
	<p><?= $condominium->getName(); ?> is located at <?= $condominium->getAddress();?></p>

	<?php $condominium->setName('Enzo Tower') ; ?>
	<p>The name of the building has been changed to <?= $condominium->getName(); ?></p>
</body>
</html>
